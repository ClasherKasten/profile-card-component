module.exports = {
	content: ["./src/**/*.{html,js,css}"],
	theme: {
		extend: {
			fontFamilys: {
				"pc-ks": ["Kumbh Sans"]
			},
			backgroundImage: {
				'bg-top': "url('../../images/bg-pattern-top.svg')",
				'bg-bottom': "url('../../images/bg-pattern-bottom.svg')"
			},
			colors: {
				"pc-dark-cyan": "hsl(185, 75%, 39%)",
				"pc-very-dark-desaturated-blue": "hsl(229, 23%, 23%)",
				"pc-dark-grayish-blue": "hsl(227, 10%, 46%)",
				"pc-dark-gray": "hsl(0, 0%, 59%)"
			}
		},
	},
	plugins: [],
}
